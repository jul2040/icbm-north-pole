extends "res://game/Level.gd"

var objects = [preload("res://game/Player.tscn"),
		preload("res://game/Planet.tscn"),
		preload("res://game/Moon.tscn"),
		preload("res://game/Target.tscn"),
		preload("res://game/Hole.tscn"),
		preload("res://game/SmallHole.tscn"),
		preload("res://game/Wall.tscn")]

func _setup():
	var data = Marshalls.base64_to_variant(StringCompressor.decompress(Serializer.get("editor_level")))
	if(not data is Array):
		get_tree().change_scene("res://menus/MainMenu.tscn")
		return
	$Player.queue_free()
	$Target.queue_free()
	for i in range(0, len(data), 3):
		var ind = data[i]
		var object = objects[ind].instance()
		object.position = data[i+1]
		object.rotation = data[i+2]
		if(ind in [1, 2, 4, 5]):
			$Planets.add_child(object)
		elif(ind == 6):
			object.scale = Vector2(1, 50)
			$Obstacles.add_child(object)
		else:
			add_child(object)
		if(ind == 0):
			player = object
			player.connect("max_fuel", $CanvasLayer/UI, "_on_Player_max_fuel")
			player.connect("update_fuel", $CanvasLayer/UI, "_on_Player_update_fuel")
			player.connect("win", $CanvasLayer/UI, "_on_Player_win")
			player.connect("crash", self, "_on_Player_crash")
	if(not is_instance_valid(player)):
		get_tree().change_scene("res://menus/MainMenu.tscn")
		return
	#UI stuff
	$CanvasLayer/UI/Win/NextLevel.text = "Copy Code"
