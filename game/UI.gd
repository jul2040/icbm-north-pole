extends Control

func _on_Player_update_fuel(f):
	$FuelBar.value = f

func _on_Player_max_fuel(f):
	$FuelBar.max_value = f

func _on_Player_win():
	$FuelBar.visible = false
	$Win.visible = true
	if(is_instance_valid(get_node_or_null("Win/NextLevel"))):
		$Win/NextLevel.grab_focus()
	else:
		$Win/MainMenu.grab_focus()

var level_number:int
func _on_NextLevel_pressed():
	if($Win/NextLevel.text == "Copy Code" or $Win/NextLevel.text == "Copied!"):
		OS.clipboard = Serializer.get("editor_level")
		$Win/NextLevel.text = "Copied!"
	else:
		get_tree().change_scene("res://game/levels/Level%d.tscn"%(level_number+1))

func _on_Replay_pressed():
	get_tree().reload_current_scene()

func _on_MainMenu_pressed():
	get_tree().change_scene("res://menus/MainMenu.tscn")
