extends Control

func _process(_delta):
	if(Input.is_action_just_pressed("pause")):
		set_paused(not get_tree().paused)

func _on_Unpause_pressed():
	set_paused(false)

func set_paused(p:bool):
	get_tree().paused = p
	visible = p
	if(p):
		$Unpause.grab_focus()

func _on_Reset_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()

func _on_MainMenu_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://menus/MainMenu.tscn")
