extends Area2D

export(float) var mass = 50000000
export(float) var atm_radius = 600
export(float) var air_resistance = 0.1
export(float) var planet_radius = 400

export(Color) var color = Color(0, 0.683594, 0.234985)
export(Color) var atm_color = Color(0.480469, 0.75647, 1, 0.509804)

onready var radius = $Planet/CollisionShape2D.shape.radius
func _on_PlanetGravity_body_entered(body):
	if(body.is_in_group("player")):
		body.planets.append(self)

func _on_PlanetGravity_body_exited(body):
	if(body.is_in_group("player")):
		body.planets.erase(self)

func _ready():
	$Atmosphere.radius = atm_radius
	$Atmosphere.damp = air_resistance
	$Planet/CollisionShape2D.shape.radius = planet_radius
	$Planet.color = color
	$Planet.update()
	$Atmosphere.color = atm_color
	$Atmosphere.update()
