extends Label

func input_to_string(input_event: InputEvent) -> String:
	if input_event is InputEventMouseButton:
			if input_event.button_index == BUTTON_LEFT:
				return "Mouse Left"
			elif input_event.button_index == BUTTON_RIGHT:
				return "Mouse Right"
			elif input_event.button_index == BUTTON_MIDDLE:
				return "Mouse Middle"
			else:
				return "Mouse Button"
	else:
		return input_event.as_text()

export(Array, String) var actions = []
func _ready():
	var events = []
	for a in actions:
		events.append(input_to_string(InputMap.get_action_list(a)[0]))
	text = text%events
