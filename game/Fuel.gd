extends Area2D

func _on_Fuel_body_entered(body):
	if(body.is_in_group("player")):
		body.reset_fuel()
		queue_free()
