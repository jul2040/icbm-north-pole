extends KinematicBody2D

export(float) var engine_accel = 600 # pixels/s^2
export(float) var rot_accel = 10 # rad/s^2
export(float) var angular_damp = 2
export(float) var explode_velocity = 20
export(float) var fuel = 3

signal max_fuel(f)
onready var max_fuel = fuel
func _ready():
	emit_signal("max_fuel", fuel)

func reset_fuel():
	fuel = max_fuel
	emit_signal("update_fuel", fuel)

func _init():
	explode_velocity = pow(explode_velocity, 2) # so we can save time with squared distance

signal crash

var exploding:bool = false
var win:bool = false
func explode():
	if(exploding):
		return
	emit_signal("crash")
	$EnginePlayer.playing = false
	$ExplodePlayer.play()
	exploding = true
	$Visuals.visible = false
	$Path.visible = false
	$ExplodeParticles.emitting = true
	velocity = Vector2()
	set_physics_process(false)
	$DieTimer.start()
	if(is_instance_valid(target)):
		target.hit()
		win = true

signal win
var target
func _on_DieTimer_timeout():
	if(win):
		$ChimePlayer.play()
		emit_signal("win")
	else:
		get_tree().reload_current_scene()

var planet_mass = 0
var planet_pos = Vector2(0, 0)
var planet_radius = 0

var velocity = Vector2()
var angular_velocity = 0

var is_colliding = false

const curve_points = 500
const curve_freq = 0.3

signal update_fuel(f)

var atmospheres = []
func resistance(pos:Vector2):
	var r = 0
	for atm in atmospheres:
		r += atm.resistance(pos)
	return r

var planets = []
func _physics_process(delta):
	if(position.length_squared() == 0):
		position = Vector2.UP
	for planet in planets:
		velocity += position.direction_to(planet.position)*(delta*(planet.mass/(position.distance_squared_to(planet.position))))
	if(fuel > 0 and Input.is_action_pressed("engine")):
		velocity += (delta*engine_accel)*Vector2.UP.rotated(rotation)
		fuel -= delta
		emit_signal("update_fuel", fuel)
		$EnginePlayer.volume_db = linear2db(1)
		$Visuals/Jet.visible = true
	else:
		$EnginePlayer.volume_db = linear2db(0)
		$Visuals/Jet.visible = false
	if(not is_colliding):
		if(Input.is_action_pressed("right")):
			angular_velocity += rot_accel*delta
		if(Input.is_action_pressed("left")):
			angular_velocity -= rot_accel*delta
	angular_velocity -= angular_velocity*delta*angular_damp
	velocity -= velocity*(delta*resistance(position))
	rotation += angular_velocity*delta
	var new_velocity = move_and_slide(velocity)
	if(get_slide_count() > 0):
		is_colliding = true
		if(velocity.length_squared() > explode_velocity):
			explode()
	else:
		is_colliding = false
	if(is_colliding):
		velocity = Vector2()
	else:
		velocity = new_velocity
	#draw curve
	var points = []
	var cur = position
	var cur_v = velocity
	for i in range(curve_points):
		var next = move(float(i*curve_freq)/curve_points, cur, cur_v)
		cur = next[0]
		cur_v = next[1]
		points.append(cur-position)
	$Path.points = points

func _process(_delta):
	$Path.rotation = -rotation

func move(delta:float, pos:Vector2, vel:Vector2):
	var final = pos
	var v = vel
	if(pos.length_squared() == 0):
		pos = Vector2.UP
	v -= v*(delta*(resistance(pos)))
	for planet in planets:
		v += pos.direction_to(planet.position)*(delta*(planet.mass/(pos.distance_squared_to(planet.position))))
	final += v*delta
	for planet in planets:
		if(final.distance_squared_to(planet.position) < pow((planet.radius-30), 2)):
			#stop moving if intersecting with planet
			final = pos
			v = Vector2()
			break
	return [final, v]
