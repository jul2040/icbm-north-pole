extends Area2D

func _on_Target_body_entered(body):
	if(body.is_in_group("player")):
		body.target = self

func _on_Target_body_exited(body):
	if(body.is_in_group("player")):
		body.target = null

func hit():
	$BellsPlayer.play()
	$House.visible = false
