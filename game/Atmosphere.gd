extends Area2D

export(float) var damp = 0.1
export(float) var radius = 600

var color = Color(0.480469, 0.75647, 1, 0.509804)
func _draw():
	draw_circle(Vector2(), radius, color)

func resistance(pos:Vector2):
	var height = pos.distance_to(global_position)
	if(height == 0):
		return 0
	return clamp(lerp(damp, 0, (((height)/radius))), 0, damp)

func _on_Atmosphere_body_entered(body):
	if(body.is_in_group("player")):
		body.atmospheres.append(self)

func _on_Atmosphere_body_exited(body):
	if(body.is_in_group("player")):
		body.atmospheres.erase(self)
