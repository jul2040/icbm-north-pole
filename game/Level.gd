extends Node2D

export(int) var level_number = 1
export(bool) var last_level = false

var player
func _ready():
	if(last_level):
		$CanvasLayer/UI/Win/NextLevel.queue_free()
	player = $Player
	$CanvasLayer/UI.level_number = level_number
	_setup()

func _setup():
	pass

func _process(_delta):
	var closest
	var closest_dist
	for p in $Planets.get_children():
		var d = p.global_position.distance_to(player.global_position)
		if(closest_dist == null or d < closest_dist):
			closest_dist = d
			closest = p
	if(closest == null):
		return
	$Camera2D.zoom.x = clamp(closest_dist*0.004, 2, 4)
	$Camera2D.zoom.y = $Camera2D.zoom.x
	$Camera2D.position = lerp(closest.position, player.position, clamp(closest_dist*0.001, 0, 1))
	if(Input.is_action_just_pressed("reset")):
		get_tree().reload_current_scene()

var shake = 0
const num_shakes = 10
func _on_Player_crash():
	$ShakeTimer.start()

func _on_ShakeTimer_timeout():
	shake += 1
	if(shake <= num_shakes):
		$Camera2D.offset = Vector2.UP.rotated(rand_range(0, 2*PI))*10
	else:
		shake = 0
		$ShakeTimer.stop()
		$Camera2D.offset = Vector2()
