extends Node2D

func _ready():
	$CanvasLayer/UI/Play.grab_focus()
	if(Serializer.get("editor_level") != null):
		load_string(Serializer.get("editor_level"))

func get_selected_object():
	for selector in $CanvasLayer/UI/Objects.get_children():
		if(selector.pressed):
			return selector

func _unhandled_input(event):
	if(event is InputEventMouseMotion):
		var pos = event.position
		if(pos.x > $CanvasLayer/UI/Objects.rect_size.x):
			$Preview.visible = true
			var selected = get_selected_object()
			if(selected != null):
				$Preview.scale.x = selected.world_scale
				$Preview.scale.y = $Preview.scale.x
				$Preview.texture = selected.texture_normal
				$Preview.position = get_global_mouse_position()
			else:
				$Preview.visible = false
		else:
			$Preview.visible = false
	elif(event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed):
		var selector = get_selected_object()
		if(selector != null):
			var go = preload("GameObject.tscn").instance()
			go.position = get_global_mouse_position()
			go.texture = selector.texture_normal
			if(scales[selector.object_ind] is Vector2):
				go.scale = scales[selector.object_ind]
			else:
				go.scale.x = selector.world_scale
				go.scale.y = go.scale.x
			go.object_ind = selector.object_ind
			$GameObjects.add_child(go)

func _on_Play_pressed():
	Serializer.set("editor_level", serialize())
	get_tree().change_scene("res://game/EditorLevel.tscn")

func _on_Exit_pressed():
	Serializer.set("editor_level", serialize())
	get_tree().change_scene("res://menus/MainMenu.tscn")

func serialize()->String:
	var data = []
	for o in $GameObjects.get_children():
		data.append(o.object_ind)
		data.append(o.position)
		data.append(o.rotation)
	return StringCompressor.compress(Marshalls.variant_to_base64(data))

var textures = [preload("res://assets/missile.png"), preload("res://assets/earth.png"), preload("res://assets/moon.png"), preload("res://assets/glacier.png"), preload("res://assets/hole.png"), preload("res://assets/small_hole.png"), preload("res://assets/wall.png")]
var scales = [0.25, 0.4, 0.5, 0.4, 5, 2, Vector2((20.0/200.0), 5)]
func load_string(string:String):
	var data = Marshalls.base64_to_variant(StringCompressor.decompress(string))
	if(not data is Array):
		print("bad data")
		return
	for object in $GameObjects.get_children():
		object.queue_free()
	for i in range(0, len(data), 3):
		var ind = data[i]
		var object = preload("GameObject.tscn").instance()
		object.object_ind = ind
		object.texture = textures[ind]
		object.position = data[i+1]
		object.rotation = data[i+2]
		if(scales[ind] is Vector2):
			object.scale = scales[ind]
		else:
			object.scale.x = scales[ind]
			object.scale.y = object.scale.x
		if(ind == 0):
			object.deletable = false
		$GameObjects.add_child(object)
