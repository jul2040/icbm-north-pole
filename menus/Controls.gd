extends Control

func _on_Back_pressed():
	get_tree().change_scene("res://menus/Settings.tscn")

func _ready():
	if(visible):
		yield(get_tree(), "idle_frame")
		$ScrollContainer.scroll_vertical = $ScrollContainer.get_v_scrollbar().max_value
		$Back.grab_focus()
