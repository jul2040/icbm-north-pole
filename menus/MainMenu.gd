extends Control

func _ready():
	#instance and delete some menus to apply any changes
	for scene in [preload("res://menus/Controls.tscn"), preload("res://menus/Settings.tscn")]:
		var c = scene.instance()
		c.visible = false
		add_child(c)
		c.queue_free()
	$Play.grab_focus()

func _on_Play_pressed():
	get_tree().change_scene("res://game/levels/Level1.tscn")

func _on_LevelEditor_pressed():
	if($LineEdit.text != ""):
		Serializer.set("editor_level", $LineEdit.text)
	get_tree().change_scene("res://menus/LevelEditor.tscn")

func _on_Settings_pressed():
	get_tree().change_scene("res://menus/Settings.tscn")
