extends Sprite

export(bool) var deletable = true
export(int) var object_ind = 0

func _ready():
	var size = texture.get_size()
	$Button/CollisionShape2D.shape.extents.x = size.x*scale.x
	$Button/CollisionShape2D.shape.extents.y = size.y*scale.y

var drag = false
func _on_Button_input_event(_viewport, event, _shape_idx):
	if(event is InputEventMouseButton):
		if(event.button_index == BUTTON_RIGHT and event.pressed and deletable):
			queue_free()
		if(event.button_index == BUTTON_LEFT):
			drag = event.pressed

func _input(event):
	if(event is InputEventMouseButton and event.button_index == BUTTON_LEFT and not event.pressed):
		drag = false
	if(event is InputEventMouseMotion and drag):
		rotation = Vector2.UP.angle_to(position-get_global_mouse_position())
