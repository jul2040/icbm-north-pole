extends Control

func _ready():
	OS.window_fullscreen = Serializer.get("fullscreen")
	$Back.grab_focus()
	if(OS.get_name() == "HTML5"):
		$ToggleFullscreen.queue_free()

func _on_Controls_pressed():
	get_tree().change_scene("res://menus/Controls.tscn")

func _on_Back_pressed():
	get_tree().change_scene("res://menus/MainMenu.tscn")

func _on_ToggleFullscreen_pressed():
	OS.window_fullscreen = not OS.window_fullscreen
	Serializer.set("fullscreen", OS.window_fullscreen)
