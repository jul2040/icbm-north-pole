extends Camera2D

var drag = false
var initPosCam = false
var initPosMouse = false
var initPosNode = false

const zoom_step = 0.1

#Input handler, listen for ESC to exit app
func _input(event):
	if (event is InputEventMouseMotion):
		if(drag == true):
			var mouse_pos = get_viewport().get_mouse_position()
			var dist_x = (mouse_pos.x - initPosMouse.x)*zoom.x
			var dist_y = (mouse_pos.y - initPosMouse.y)*zoom.y
			var nx = initPosNode.x - (0 + dist_x)
			var ny = initPosNode.y - (0 + dist_y)
			position = (Vector2(nx,ny))
	if (event is InputEventMouseButton):
		if(event.button_index == BUTTON_WHEEL_UP):
			if(zoom.x > 2*zoom_step and zoom.y > 2*zoom_step):
				zoom.x = zoom.x - zoom_step
				zoom.y = zoom.y - zoom_step
		elif(event.button_index == BUTTON_WHEEL_DOWN):
			if(zoom.x < 10 and zoom.x < 10):
				zoom.x = zoom.x + zoom_step
				zoom.y = zoom.y + zoom_step
		elif(event.button_index == BUTTON_MIDDLE):
			if(event.pressed):
				initPosMouse = get_viewport().get_mouse_position()
				initPosNode = position
				drag = true
			else:
				drag = false
