extends TextureButton

export(float) var world_scale = 1
export(int) var object_ind = 0

func _draw():
	if(pressed):
		draw_rect(Rect2(Vector2(), rect_size), Color(1, 1, 1, 0.333333), true)

func _on_ObjectSelector_toggled(button_pressed):
	if(button_pressed):
		for sibling in get_parent().get_children():
			if(sibling != self):
				sibling.unpress()
	update()

func unpress():
	pressed = false
	update()
